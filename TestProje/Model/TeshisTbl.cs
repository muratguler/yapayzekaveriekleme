namespace TestProje.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TeshisTbl")]
    public partial class TeshisTbl
    {
        public int ID { get; set; }

        public int? UstKategoriID { get; set; }

        public string Adi { get; set; }

        public bool? Durum { get; set; }
    }
}
