﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace TestProje.Model
{
  public  class Methods
    {
        public static string veritabani = ConfigurationManager.ConnectionStrings["veritabani"].ConnectionString;
        public static SqlConnection Baglanti()
        {
            HttpContext httpContext = HttpContext.Current;

            if (httpContext == null)
            {
                return new SqlConnection(Methods.veritabani);
            }
            else
            {
                string key = "CurDbContext"; //Per request 
                SqlConnection context = httpContext.Items[key] as SqlConnection;

                if (context == null)
                {
                    context = new SqlConnection(Methods.veritabani);
                    httpContext.Items[key] = context;
                }

                return context;
            }
        }

        public static bool Ekle<T>(T data, bool ilkKolonuAtla = true)
        {
            bool gelenDeger = ilkKolonuAtla;
            var liste = Methods.Baglanti().Query<string>("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'" + typeof(T).Name + "'", commandTimeout: 60000).ToList();
            var insert = "INSERT INTO " + typeof(T).Name + "(";
            foreach (var kolon in liste)
            {
                if (ilkKolonuAtla)
                {
                    ilkKolonuAtla = false;
                    continue;
                }
                insert += kolon + ",";
            }

            insert = insert.Trim(',') + ") Values(";
            ilkKolonuAtla = gelenDeger;
            foreach (var kolon in liste)
            {
                if (ilkKolonuAtla)
                {
                    ilkKolonuAtla = false;
                    continue;
                }
                insert += "@" + kolon + ",";
            }
            insert = insert.Trim(',') + ")";

            var result = Methods.Baglanti().Execute(insert, data, commandTimeout: 60000);
            return result != 0;
        }
        public static bool Guncelle<T>(T data, bool ilkKolonuAtla = true)
        {

            var liste = Methods.Baglanti().Query<string>("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'" + typeof(T).Name + "'", commandTimeout: 60000).ToList();

            var update = "UPDATE " + typeof(T).Name + " set ";
            foreach (var kolon in liste)
            {
                if (ilkKolonuAtla)
                {
                    ilkKolonuAtla = false;
                    continue;
                }
                update += kolon + "=@" + kolon + ",";
            }
            update = update.Trim(',') + " where " + liste[0] + "=@" + liste[0];

            var result = Methods.Baglanti().Execute(update, data, commandTimeout: 60000);
            return result != 0;
        }
        public static List<T> Liste<T>()
        {
            var liste = Methods.Baglanti().Query<T>("SELECT * FROM " + typeof(T).Name, commandTimeout: 60000).ToList();
            return liste;
        }
        public static bool Sil<T>(T data)
        {
            var liste = Methods.Baglanti().Query<string>("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'" + typeof(T).Name + "'", commandTimeout: 60000).ToList();

            var result = Methods.Baglanti().Execute("DELETE FROM " + typeof(T).Name + " WHERE " + liste[0] + "=@" + liste[0], data, commandTimeout: 60000);
            return result != 0;
        }
        public static T GetirID<T>(T data)
        {
            var tumliste = Methods.Baglanti().Query<string>("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'" + typeof(T).Name + "'", commandTimeout: 60000).ToList();
            var liste = Methods.Baglanti().Query<T>("SELECT * FROM " + typeof(T).Name + " where " + tumliste[0] + "=@" + tumliste[0], data, commandTimeout: 60000).FirstOrDefault();
            return liste;
        }

        public static int EkleIdDondur<T>(T data, bool ilkKolonuAtla = true)
        {
            bool gelenDeger = ilkKolonuAtla;
            var liste = Methods.Baglanti().Query<string>("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'" + typeof(T).Name + "'", commandTimeout: 60000).ToList();
            var insert = "INSERT INTO " + typeof(T).Name + "(";
            foreach (var kolon in liste)
            {
                if (ilkKolonuAtla)
                {
                    ilkKolonuAtla = false;
                    continue;
                }
                insert += kolon + ",";
            }

            insert = insert.Trim(',') + ") OUTPUT Inserted.KAYIT_ID Values(";
            ilkKolonuAtla = gelenDeger;
            foreach (var kolon in liste)
            {
                if (ilkKolonuAtla)
                {
                    ilkKolonuAtla = false;
                    continue;
                }
                insert += "@" + kolon + ",";
            }
            insert = insert.Trim(',') + ")";

            var result = Methods.Baglanti().QueryFirst<int>(insert, data, commandTimeout: 60000);
            return result;
        }
    }
}
