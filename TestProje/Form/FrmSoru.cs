﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestProje.Model;

namespace TestProje.Form
{
    public partial class FrmSoru : DevExpress.XtraEditors.XtraForm
    {
        List<TeshisTbl> liste = new List<TeshisTbl>();
        int? ustKatId = null;
        public FrmSoru()
        {
            InitializeComponent();
        }

        private void FrmSoru_Load(object sender, EventArgs e)
        {
            try
            {
                liste = Methods.Liste<TeshisTbl>();
                var secim = liste.First(x => x.UstKategoriID == null);
                richTextBox1.Text = secim.Adi;
            }
            catch (Exception ex)
            {
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            try
            {
                var secim = liste.First(x => x.UstKategoriID == ustKatId && x.Durum == true);
                ustKatId = secim.ID;
                var secim2 = liste.First(x => x.UstKategoriID == ustKatId);
                richTextBox1.Text = secim2.Adi;
            }
            catch (Exception ex)
            {
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            try
            {
                var secim = liste.First(x => x.UstKategoriID == ustKatId && x.Durum == false);
                richTextBox1.Text = secim.Adi;
                ustKatId = secim.ID;
                var secim2 = liste.First(x => x.UstKategoriID == ustKatId);
                richTextBox1.Text = secim2.Adi;
            }
            catch (Exception ex)
            {
            }
        }
    }
}