﻿using System;
using TestProje.Model;
using System.Windows.Forms;
namespace TestProje.Form
{
    public partial class FrmVeriEkle : DevExpress.XtraEditors.XtraForm
    {
        int? secilenUstKategoriID = null;
        public FrmVeriEkle()
        {
            InitializeComponent();
            Listele();
        }
        void Listele()
        {
            secilenUstKategoriID = null;
            gridControl1.DataSource = Methods.Liste<TeshisTbl>();
        }
        private void btnKaydet_Click(object sender, EventArgs e)
        {
            if (chkAltKategori.Checked)
            {
                for (int i = 0; i < 2; i++)
                {
                    TeshisTbl t = new TeshisTbl();
                    t.Adi = txtBaslik.Text.Trim();
                    t.UstKategoriID = secilenUstKategoriID;
                    t.Durum = (i != 0);
                    Methods.Ekle(t);
                }
            }
            else
            {
                TeshisTbl t = new TeshisTbl();
                t.Adi = txtBaslik.Text.Trim();
                t.UstKategoriID = secilenUstKategoriID;
                t.Durum = null;
                Methods.Ekle(t);
            }
            txtBaslik.Text = "";
            lblKayitID.Text = "";
            Listele();
        }
        private void kayitEklenecekToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var secim = gridView1.GetFocusedRowCellValue(colID);
            if (secim != null)
            {
                secilenUstKategoriID = int.Parse(secim.ToString());
                lblKayitID.Text = secilenUstKategoriID + "-" + gridView1.GetFocusedRowCellValue(colAdi);
            }
        }
        private void gridView1_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
                contextMenuStrip1.Show(Cursor.Position.X, Cursor.Position.Y);
        }
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            FrmSoru frm = new FrmSoru();
            frm.ShowDialog();
        }
    }
}